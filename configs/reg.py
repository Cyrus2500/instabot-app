import os
from configs.path import BASE_PATH


def register():
    os.chdir(BASE_PATH)
    try:
        os.mkdir("data")
        return True
    except FileExistsError:
        return False


def unregister():
    try:
        os.rmdir(os.path.join(BASE_PATH, "data"))
    except FileNotFoundError:
        pass
