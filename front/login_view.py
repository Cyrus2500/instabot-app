import os
import json
import threading
import customtkinter as tk
import tkinter
import requests
from backend.utils.instagram import Instagram
from front.utils.messagesBox import MessageBoxClass
from front.utils.image_utils import (
    PASSWORD_HIDE_IMAGE_BLACK,
    PASSWORD_VIEW_IMAGE_BLACK,
    PASSWORD_VIEW_IMAGE_WHITE,
    PASSWORD_HIDE_IMAGE_WHITE,
)
from front.utils.constructor_class import ConstructorClass
from configs.reg import register, unregister
from configs.path import BASE_PATH
from front.utils.decorators import user_pass_validator


class LoginView(ConstructorClass):
    """
    LoginView class for creating the login interface of an Insta bot application.

    Key features:
        GUI creation: Builds the login view using CustomTkinter elements.
        User input: Collects username and password from the user.
        Password visibility toggle: Allows users to show or hide their password.
        Login functionality: Handles login attempts using Instagram API calls.
        Error handling: Manages errors gracefully, displaying informative messages.
        Data persistence: Stores user information for subsequent sessions.
        View management: Transitions to the next view upon successful login.

    Methods:
        init(self, app, next_view): Initializes the class and sets up GUI elements.
        create_view(self): Creates and positions GUI elements on the screen.
        __get_data(self): Attempts to load user data from a file if available.
        __get_next_view(self, site_data): Switches to the next view, passing site data.
        __change_state_password(self): Toggles password visibility.
        __login(self): Initiates the login process using a separate thread.
        __on_login(self, username, password): Performs the actual login attempt.

    Usage:
        Create an instance of the LoginView class, passing the main application object and the next view to be displayed.
        Call the create_view() method to construct the login interface.
    """

    __hide_image = tk.CTkImage(
        dark_image=PASSWORD_HIDE_IMAGE_WHITE,
        light_image=PASSWORD_HIDE_IMAGE_BLACK,
        size=(16, 16),
    )
    __view_image = tk.CTkImage(
        dark_image=PASSWORD_VIEW_IMAGE_WHITE,
        light_image=PASSWORD_VIEW_IMAGE_BLACK,
        size=(16, 16),
    )

    def __init__(self, app, next_view):
        self.__app = app
        self.__next_view = next_view
        self.site_data = None
        self.__frame = tk.CTkFrame(self.__app, width=250, height=350)
        self.__font = tk.CTkFont(family="Times New Roman", size=30, weight="bold")
        self.__app_name = tk.CTkLabel(self.__frame, text="Instabot", font=self.__font)

        self.__username = tk.CTkEntry(self.__frame, placeholder_text="username")
        self.__password = tk.CTkEntry(
            self.__frame, placeholder_text="password", show="*"
        )

        self.__status_label = tk.CTkLabel(self.__frame, text="", text_color="red")

        self.__state_password_btn = tk.CTkButton(
            self.__frame,
            text="",
            image=self.__hide_image,
            width=15,
            height=15,
            bg_color="transparent",
            fg_color="transparent",
            hover=False,
            corner_radius=100,
            command=lambda: self.__change_state_password(),
        )
        self.__login_button = tk.CTkButton(
            self.__frame,
            text="Login",
            command=lambda: self._login(),
            text_color=("black", "white"),
        )
        self.__try_again_login_button = tk.CTkButton(
            self.__frame,
            text="Try again",
            text_color=("black", "white"),
            command=lambda: self._try_again(),
        )

        self.canvas: list = self.canvas_creator(
            self.__frame,
            width=200,
            height=6,
            bg="gray86",
            highlight_background="gray86",
            total=1,
        )

    def create_view(self):
        """Packs and positions GUI elements on the screen."""
        self.__frame.pack(expand=True)
        self.__app_name.place(anchor=tk.S, relx=0.5, rely=0.3)
        self.canvas[0].place(anchor=tk.CENTER, relx=0.5, rely=0.32)
        self.canvas[0].create_line(3, 5, 200, 5, width=1, fill="gray55")
        self.__status_label.place(anchor=tkinter.S, relx=0.5, rely=0.83)

        if register():
            self.__username.place(anchor=tkinter.S, relx=0.5, rely=0.65)
            self.__password.place(anchor=tkinter.S, relx=0.5, rely=0.75)
            self.__state_password_btn.place(anchor=tkinter.S, relx=0.85, rely=0.75)
            self.__login_button.place(anchor=tkinter.N, relx=0.5, rely=0.85)

        else:
            self._load_data()

    def __get_next_view(self, site_data):
        self.__frame.pack_forget()
        self.__next_view.get_data(site_data)
        self.__next_view.create_view()

    def __change_state_password(self):
        """Toggles password visibility."""
        if self.__password.cget("show") == "":
            self.__password.configure(show="*")
            self.__state_password_btn.configure(image=self.__hide_image)
        else:
            self.__password.configure(show="")
            self.__state_password_btn.configure(image=self.__view_image)

    def _try_again(self):
        """Try again for load info from session file."""
        self.__status_label.configure(text="Trying again")
        threading.Thread(target=self.__on_load, daemon=True).start()

    def _load_data(self):
        """Load info from session file."""
        self.__status_label.configure(text="loading info ...")
        threading.Thread(target=self.__on_load, daemon=True).start()

    def __on_load(self):
        """
        Attempts to load user data from a file if available.
        Steps:
            1. Loads session data from the file.
            2. Loads user information into Instagram data manager.
            3. Gets an instance of the Instagram user class.
            4. Initializes site data.
            5. Transitions to the next view.

        Warning:
            some error fixed ...
        """
        try:
            with open(os.path.join(BASE_PATH, "data", "UserInfo.json"), "r") as file:
                data = json.loads(file.read())
                Instagram.data_manager.load_info(data.get("user"))

                instance = Instagram.initial(data.get("user"))
                self.site_data = instance.initial_site_data("file", store=False)
                self.__get_next_view(self.site_data)
        except Instagram.exceptions.ConnectionException:
            MessageBoxClass.raise_box_error(
                "Connection error", "Check your internet connection and try again!."
            )
            self.__try_again_login_button.place(anchor=tkinter.N, relx=0.5, rely=0.85)
        except Exception as err:
            MessageBoxClass.raise_box_error("Unknown Error", str(err))
            self.__try_again_login_button.place(anchor=tkinter.N, relx=0.5, rely=0.85)

    def _login(self):
        """Initiates the login process using a separate thread."""
        print("login started")  # watch process in the terminal
        threading.Thread(
            target=self.__on_login,
            args=(self.__username.get(), self.__password.get()),
            daemon=True,
        ).start()

    @user_pass_validator
    def __on_login(self, username, password):
        """Performs the actual login attempt."""
        register()
        print(username, password)  # watch process in the terminal
        self.__login_button.configure(state=tk.DISABLED)

        try:
            Instagram.login(user=username, passw=password)
        except requests.exceptions.ConnectTimeout:
            MessageBoxClass.raise_box_error(
                "Connection failed",
                message="Connection timed out to instagram; check your internet connection",
            )
            unregister()

        except Instagram.exceptions.ConnectionException as error:
            if "your password was incorrect" in str(error):
                MessageBoxClass.raise_box_error(
                    "Wrong password",
                    "Sorry, your password was incorrect. Please double-check your password.",
                )
            else:
                MessageBoxClass.raise_browser_error()
            unregister()

        except Exception as error:
            MessageBoxClass.raise_box_error("Unknown error", message=str(error))
            unregister()
        else:
            self.site_data = Instagram.initial(self.__username.get()).initial_site_data(
                "file", store=True
            )
            Instagram.data_manager.save_info()
            self.__get_next_view(self.site_data)

        self.__login_button.configure(state=tk.NORMAL)
