import threading
import customtkinter as tk
from front.utils.image_utils import profile_image_getter
from front.utils.constructor_class import ConstructorClass
from backend.utils.instagram import Instagram
from front.utils.messagesBox import MessageBoxClass
from front.utils.events import EventHandlerMixin
from front.utils.toplevel_class import ToplevelWindow


class HomeView(ConstructorClass):
    """
    HomeView class that constructs the main content view of the application,
     displaying user information and follow lists related to their Instagram account.

    Key components:
        Initialization:
            Sets up the main frame and UI elements upon creation.
            Handles the window closing event.
        Data Handling:
            Receives a reference to the menu frame for communication.
            Populates the view with user data and creates UI elements.
            Downloads and displays the user's profile picture.
            Configures labels with user data.
            Retrieves and displays counts for each follow list category.
        Follow List Handling:
            Triggers the display of specific follow lists in separate top-level windows.

    Attributes:
        app (customtkinter.CTk): The main application window.
        menu_frame (MenuFrame): The menu frame for accessing settings and other options.
        site_data (dict): Stores user's Instagram data.
        toplevel_window (ToplevelWindow): The top-level window used for displaying detailed follow lists.

        Fonts elements:
            username_font: Font for displaying the username label.
            number_font: Font for displaying numbers labels such as post, followers, followings number.

        UI Elements:
            main_frame: The main frame containing the home view elements.
            profile_label: Displays the user's profile picture.
            username_label: Displays the user's username.
            post_count_label, followers_count_label, followings_count_label: Display count information.
            canvas: A separator line between the profile section and follow list sections.
            frames: A list of frames containing follow list information.
            buttons: A list of buttons to trigger viewing detailed follow lists.
            title_labels: A list of labels displaying the titles of follow list sections.

    Methods:
        get_menu_frame(self, frame): Receives a reference to the menu frame for communication.
        create_view(self, site_data): Populates the view with user data and creates UI elements.
        __load_profile(self): Downloads and displays the user's profile picture.
        __all_configue_labels(self): Configures labels with user data and initiates profile loading.
        __pack_repetitions_objects(self): Packs repetitive UI elements (frames, buttons, labels).
        __get_info_counts(self): Retrieves and displays counts for each follow list category.
        unfollow_list_view, none_follow_list_view, mutual_follow_list_view, new_follow_list_view: Triggers the display of
         respective follow lists in a top-level window.
    """
    PROFILE = None
    __MY_CANVAS_Y = 255
    __MY_FRAME_Y = 200

    def __init__(self, app):
        self.app = app
        self.menu_frame = None
        self.site_data = None
        self.toplevel_window = None
        self.username_font = tk.CTkFont(family="Times New Roman", size=18, weight="bold", underline=True)
        self.number_font = tk.CTkFont(family="Times New Roman", size=14, weight="bold")

        self.app.protocol("WM_DELETE_WINDOW", lambda: EventHandlerMixin.on_close(self.app, self.site_data))
        self.main_frame = tk.CTkFrame(self.app, fg_color="transparent", corner_radius=0)
        self.profile_label = tk.CTkLabel(self.main_frame, text="NONE",
                                         bg_color="transparent", fg_color="transparent")
        self.username_label = tk.CTkLabel(
            self.main_frame, text_color=("black", "white"),
            text="None", font=self.username_font, bg_color="transparent", fg_color="transparent", corner_radius=5
        )

        self.post_count_label = tk.CTkLabel(
            self.main_frame, text_color=("black", "white"), height=15, text="0",
            bg_color="transparent", fg_color="transparent", font=self.number_font, corner_radius=5)
        self.followers_count_label = tk.CTkLabel(
            self.main_frame, text_color=("black", "white"), height=15, text="0",
            bg_color="transparent", fg_color="transparent", font=self.number_font, corner_radius=5
        )
        self.followings_count_label = tk.CTkLabel(
            self.main_frame, text_color=("black", "white"), height=15, text="0",
            bg_color="transparent", fg_color="transparent", font=self.number_font, corner_radius=5
        )

        self.posts_label = tk.CTkLabel(
            self.main_frame, text_color=("black", "white"),
            text="posts", bg_color="transparent", fg_color="transparent", corner_radius=5)
        self.followers_label = tk.CTkLabel(
            self.main_frame, text_color=("black", "white"),
            text="followers", bg_color="transparent", fg_color="transparent", corner_radius=5
        )
        self.followings_label = tk.CTkLabel(
            self.main_frame, text_color=("black", "white"),
            text="followings", bg_color="transparent", fg_color="transparent", corner_radius=5
        )

        self.canvas = tk.CTkCanvas(
            master=self.main_frame, width=375, height=6, bg="gray92", highlightbackground="gray92"
        )
        self.frames = self.frame_creator(
            self.main_frame, total=4, width=350, height=40
        )
        self.commands = [
            self.unfollow_list_view, self.none_follow_list_view, self.mutual_follow_list_view, self.new_follow_list_view
        ]
        self.buttons = self.buttons_creator(
            frames=self.frames, commands=self.commands, width=32, text="view list >"
        )
        title_texts = ["un followers", "none followers", "mutual followers", "new followers"]
        self.title_labels = self.label_creator(
            self.frames, texts=title_texts, anchor=tk.E, corner_radius=10
        )

    def get_menu_frame(self, frame):
        """
        Getting the base frame in the RootView class just to access the setting menu buttons
         and initiate self.menu_frame
        """
        self.menu_frame = frame

    def get_data(self, data):
        self.site_data = data

    def create_view(self):
        """Packs and positions GUI elements on the screen."""
        self.menu_frame.disable_buttons(direct_button=True)
        self.main_frame.pack(fill="both", expand=True)
        self.profile_label.place(anchor=tk.CENTER, x=90, y=100)

        self.post_count_label.place(anchor=tk.CENTER, x=180, y=102)
        self.followers_count_label.place(anchor=tk.CENTER, x=260, y=102)
        self.followings_count_label.place(anchor=tk.CENTER, x=355, y=102)

        self.username_label.place(anchor=tk.N, x=190, y=55)
        self.posts_label.place(anchor=tk.CENTER, x=180, y=125)
        self.followers_label.place(anchor=tk.CENTER, x=260, y=125)
        self.followings_label.place(anchor=tk.CENTER, x=355, y=125)
        self.canvas.place(anchor=tk.CENTER, relx=0.5, rely=0.32)
        self.canvas.create_line(3, 5, 375, 5, width=1, fill="gray55")

        self.__all_configue_labels()
        self.__pack_repetitions_objects()
        self.__get_info_counts()

    def __load_profile(self):
        """Downloads and displays the user's profile picture and configure as profile_label"""
        print("downloading profile ...")  # watch process in the terminal
        res, msg = Instagram.downloader.download_only_profile(self.username_label.cget("text"))
        if res:
            print("packing profile")  # watch process in the terminal
            self.PROFILE = profile_image_getter()
            self.profile_label.configure(image=tk.CTkImage(self.PROFILE, size=(110, 110)), text="")
        else:
            MessageBoxClass.raise_box_error('profile download error', msg)

    def __all_configue_labels(self):
        """Configures labels with user data and initiates profile loading."""
        self.username_label.configure(text=self.site_data.get("user", "Username"))
        self.post_count_label.configure(text=f"{self.site_data.get("post_count", 0)}")
        self.followers_count_label.configure(text=f"{self.site_data.get("followers_count", 0)}")
        self.followings_count_label.configure(text=f"{self.site_data.get("following_count", 0)}")

        # self.__load_profile()
        threading.Thread(target=self.__load_profile, daemon=True).start()

    def __pack_repetitions_objects(self):
        """Packs repetitive UI elements (frames, buttons, labels)."""
        for frame in self.frames:
            frame.place(x=25, y=self.__MY_FRAME_Y)
            self.__MY_FRAME_Y += 60

        for btn in self.buttons:
            btn.place(anchor=tk.CENTER, relx=0.9, rely=0.5)

        for label in self.title_labels:
            label.place(x=10, y=8)

    def __get_info_counts(self):
        """Retrieves and displays counts for each follow list category."""
        un_follow_count = str(Instagram.analyzer(self.site_data, "file").unfollow_count())
        none_follow_count = str(Instagram.analyzer(self.site_data, "file").none_follow_count())
        mutual_follow_count = str(Instagram.analyzer(self.site_data, "file").mute_follow_count())
        new_follow_count = str(Instagram.analyzer(self.site_data, "file").new_follow_count())

        count_texts = [un_follow_count, none_follow_count, mutual_follow_count, new_follow_count]
        count_labels = self.label_creator(
            self.frames, texts=count_texts, anchor=tk.W, corner_radius=5
        )
        for label in count_labels:
            label.place(x=225, y=8)

    def __show_list_view(self, function):
        list_data = function()
        print(list_data)
        if self.toplevel_window is None or not self.toplevel_window.winfo_exists():
            self.toplevel_window = ToplevelWindow().get_toplevel_view(list_data)
        else:
            self.toplevel_window.focus()

    def unfollow_list_view(self):
        self.__show_list_view(Instagram.analyzer(self.site_data).unfollow_list)

    def none_follow_list_view(self):
        self.__show_list_view(Instagram.analyzer(self.site_data).none_follow_list)

    def mutual_follow_list_view(self):
        self.__show_list_view(Instagram.analyzer(self.site_data).mutual_follow_list)

    def new_follow_list_view(self):
        self.__show_list_view(Instagram.analyzer(self.site_data).new_follow_list)
