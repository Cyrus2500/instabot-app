import tkinter
from front.login_view import LoginView
import customtkinter as tk
from front.home_view import HomeView
from front.utils.image_utils import (
    BLACK_MODE,
    WHITE_MODE,
    WHITE_SETTINGS_IMAGE,
    BLACK_SETTINGS_IMAGE,
)
from front.utils.menu_frame import MenuFrame


class MainView(tk.CTk):
    """
    MainView class that creates and manages the main window of the application.

    Key features:
        Window setup: Initializes the main window with title, size, and resizing behavior.
        View management: Handles transitions between login and home views.
        Menu functionality: Creates a settings menu with options for changing theme and potentially other settings.
        Theme switching: Allows users to toggle between light and dark themes.
        Animation effects: Provides a visual slide-out animation for the settings menu.

    Attributes:
        __my_x (int): Stores the x-coordinate of the settings menu frame for animation.
        __SETTINGS_IMAGE (tk.CTKImage): Holds the image for the settings button in both light and dark modes.
        __CHANGE_MODE_IMAGE (tk.CTKImage): Holds the image for the theme change button in light and dark modes.
        home_view (HomeView): An instance of the HomeView class, representing the home content view.
        login_view (LoginView): An instance of the LoginView class, handling user login.
        setting_menu_frame (tk.CTKFrame): The frame containing the settings menu.
        menu_frame (MenuFrame): An instance of the MenuFrame class, providing menu options.
        __btn_setting (tk.CTKButton): The button that triggers the settings menu to appear.
        __btn_change_mode (tk.CTKButton): The button within the settings menu that toggles the theme.

    Methods:
        init(self): Initializes the main window and its components.
        __create_view(self): Packs and positions GUI elements on the screen.
        __change_theme(self): Switches between light and dark themes, updating colors and visual elements.
        __slide_out(self): Animates the settings menu sliding out of view.
        __slide_in(self): Animates the settings menu sliding into view.
        __show_frame(self): Displays or hides the settings menu with animation.
        *mainloop(self, args, kwargs): Runs the main loop of the application, handling events and updates.
    """

    __my_x: int = -120
    __SETTINGS_IMAGE = tk.CTkImage(
        dark_image=WHITE_SETTINGS_IMAGE, light_image=BLACK_SETTINGS_IMAGE, size=(25, 25)
    )
    __CHANGE_MODE_IMAGE = tk.CTkImage(
        dark_image=WHITE_MODE, light_image=BLACK_MODE, size=(20, 20)
    )

    def __init__(self):
        super().__init__()
        self.geometry("400x550")
        self.title("Insta bot")
        self.resizable(False, False)

        self.home_view = HomeView(self)
        self.login_view = LoginView(self, self.home_view)
        self.setting_menu_frame = tk.CTkFrame(
            self,
            width=120,
            height=560,
            corner_radius=10,
            border_color=("black", "white"),
            border_width=1,
        )
        self.menu_frame = MenuFrame(self, self.setting_menu_frame, 4)
        self.home_view.get_menu_frame(self.menu_frame)

        self.__btn_setting = tk.CTkButton(
            self,
            text="",
            image=self.__SETTINGS_IMAGE,
            width=15,
            height=25,
            command=lambda: self.__show_frame(),
        )
        self.__btn_change_mode = tk.CTkButton(
            self.setting_menu_frame,
            text="light",
            text_color=("black", "white"),
            image=self.__CHANGE_MODE_IMAGE,
            height=25,
            width=95,
            compound="left",
            font=("", 10),
            command=lambda: self.__change_theme(),
            anchor=tk.W,
        )

    def __create_view(self):
        """Packs and positions GUI elements on the screen."""
        self.__btn_setting.place(anchor=tk.NW, x=3, y=15)
        self.__btn_change_mode.place(anchor=tk.W, x=8, y=80)
        self.menu_frame.create_view()
        self.menu_frame.disable_buttons(
            direct_button=True, account_button=True, logout_button=True
        )

        self.login_view.create_view()

    def __change_theme(self):
        """Change the theme of the app"""
        if self.__btn_change_mode.cget("text") == "light":
            for canvas in self.menu_frame.canvases:
                canvas.configure(bg="gray17", highlightbackground="gray17")

            self.__btn_change_mode.configure(text="night")
            self.login_view.canvas[0].configure(
                bg="gray17", highlightbackground="gray17"
            )
            self.home_view.canvas.configure(bg="gray14", highlightbackground="gray14")
            tk.set_appearance_mode("dark")
        elif self.__btn_change_mode.cget("text") == "night":
            for canvas in self.menu_frame.canvases:
                canvas.configure(bg="gray85", highlightbackground="gray85")

            self.__btn_change_mode.configure(text="light")
            self.login_view.canvas[0].configure(
                bg="gray86", highlightbackground="gray86"
            )
            self.home_view.canvas.configure(bg="gray92", highlightbackground="gray92")
            tk.set_appearance_mode("light")

    def __slide_out(self):
        """The drop-down mode of coming out of the menu"""
        self.__my_x += 5
        if self.__my_x <= -5:
            print(self.__my_x)
            self.setting_menu_frame.place(x=self.__my_x, y=48)
            self.after(10, self.__slide_out)

    def __slide_in(self):
        """Drop-down mode to enter the menu"""
        self.__my_x += -5
        if self.__my_x >= -120:
            print(self.__my_x)
            self.setting_menu_frame.place(x=self.__my_x, y=48)
            self.after(10, self.__slide_in)

    def __show_frame(self):
        """Displays the settings menu in animation mode"""
        self.__btn_setting.configure(state=tkinter.DISABLED)
        if self.__my_x <= -120:
            self.__slide_out()
        else:
            self.__slide_in()
        self.__btn_setting.configure(state=tkinter.NORMAL)

    def mainloop(self, *args, **kwargs):
        self.__create_view()
        super().mainloop(*args, **kwargs)
