import customtkinter as tk2
from typing import Generator


class ConstructorClass:
    """
    ConstructorClass that provides static methods for efficiently creating multiple instances of
     common UI elements in a consistent manner.

    Methods:

        canvas_creator(frame, total, width, height, highlight_background, bg)
        frame_creator(main_frame, total, width, height, fg_color=None, border_color=None, border_width=0)
        buttons_creator(frames, commands, text, width=None, height=25, fg_color=None, text_color=("black", "white"),
            border_color=None, hover=True)
        label_creator(frames, texts, width=35, height=25, fg_color=None, anchor=tk2.CENTER, corner_radius=0):
    """

    @staticmethod
    def canvas_creator(
        frame, total: int, width: int, height: int, highlight_background, bg
    ) -> list:
        """Creates a specified number of customtkinter.CTkCanvas objects with consistent properties.
        :argument:
            total: The number of frames to create.
            ...
        :return: A list containing the created canvas objects.
        """
        canvases = [
            tk2.CTkCanvas(
                master=frame,
                width=width,
                height=height,
                bg=bg,
                highlightbackground=highlight_background,
            )
            for _ in range(total)
        ]
        return canvases

    @staticmethod
    def frame_creator(
        main_frame,
        total: int,
        width: int,
        height: int,
        fg_color=None,
        border_color=None,
        border_width=0,
    ) -> list:
        """Creates a specified number of customtkinter.CTkFrame objects with consistent properties.
        :argument:
            main_frame: The parent frame to which the frames will be added.
            total: The number of frames to create.
            ...
        :return: A list containing the created frame objects."""
        frames = [
            tk2.CTkFrame(
                master=main_frame,
                width=width,
                height=height,
                fg_color=fg_color,
                border_color=border_color,
                border_width=border_width,
            )
            for _ in range(total)
        ]
        return frames

    @staticmethod
    def buttons_creator(
        frames: list,
        commands: list,
        text,
        width: int = None,
        height: int = 25,
        fg_color=None,
        text_color=("black", "white"),
        border_color=None,
        hover=True,
    ) -> Generator:
        """
            Creates a CTkButton for each frame in the provided list,
             assigning corresponding commands from the commands list.
        :argument:
            frames: A list of frames to place the buttons in.
            commands: A list of command functions to be assigned to the buttons.
            text: The text to display on the buttons.
            ...
        Yields: The created button objects, one for each frame."""
        command_number = 0
        for frame in frames:
            yield (
                tk2.CTkButton(
                    master=frame,
                    text=text,
                    width=width,
                    height=height,
                    fg_color=fg_color,
                    text_color=text_color,
                    border_color=border_color,
                    hover=hover,
                    command=commands[command_number],
                )
            )
            command_number += 1

    @staticmethod
    def label_creator(
        frames: list,
        texts: list,
        width: int = 35,
        height: int = 25,
        fg_color=None,
        anchor=tk2.CENTER,
        corner_radius=0,
    ) -> Generator:
        """Creates a CTkLabel for each frame in the provided list, displaying corresponding text from the texts list.
        :argument:
            frames: A list of frames to place the labels in.
            texts: A list of texts to display on the labels.
            ...

        Yields: The created label objects, one for each frame.
        """
        text_number = 0
        for frame in frames:
            yield (
                tk2.CTkLabel(
                    master=frame,
                    text=texts[text_number],
                    width=width,
                    height=height,
                    fg_color=fg_color,
                    anchor=anchor,
                    corner_radius=corner_radius,
                )
            )
            text_number += 1
