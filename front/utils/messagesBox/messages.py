from CTkMessagebox import CTkMessagebox


class MessageBoxClass:
    """
    MessageBoxClass class
        Provides a convenient interface for displaying various types of message
         boxes using the customtkinter CTkMessagebox widget.

    Methods:
        raise_browser_error()
        raise_box_error(title, message)
        raise_question(title, message)
        raise_message(title, message)
    """

    @staticmethod
    def raise_browser_error() -> None:
        """Displays a warning message box with the message Just login to your account successfully,
        through the Instagram website."""
        CTkMessagebox(
            title="browser Error",
            message="Just login to your account successfully through the Instagram website",
            width=300,
            height=150,
            button_width=20,
            button_height=15,
            icon="warning",
            icon_size=(25, 25),
            topmost=True,
            justify="center",
        )

    @staticmethod
    def raise_box_error(title, message) -> None:
        """Displays a warning message box with the specified title and message."""
        CTkMessagebox(
            title=title,
            message=message,
            width=300,
            height=150,
            button_width=20,
            button_height=15,
            icon="warning",
            icon_size=(25, 25),
            topmost=True,
            justify="center",
        )

    @staticmethod
    def raise_question(title, message):
        """Displays a question message box with the specified title and message, offering "Cancel" and "Exit" options.
        :returns: the selected option."""
        msg = CTkMessagebox(
            title=title,
            message=message,
            width=300,
            height=150,
            button_width=20,
            button_height=15,
            icon="question",
            icon_size=(25, 25),
            topmost=True,
            justify="center",
            option_1="Cancel",
            option_2="Exit",
        )
        return msg
