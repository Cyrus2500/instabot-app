import customtkinter as tk
from front.utils.constructor_class import ConstructorClass


class MenuFrame(ConstructorClass):
    """
    This module defines the MenuFrame class, responsible for constructing and managing
     a settings menu within a Tkinter application.

    Key features:
        Customizable appearance: Employs customtkinter for enhanced visual appeal.
        Clear structure: Organizes elements using labels, buttons, and canvases.
        Button control: Enables or disables specific buttons as needed.

    :Classes:
        MenuFrame(ConstructorClass)

    :Methods:
        init(self, root, frame, canvas_total=0)
            Initializes the MenuFrame instance.
        create_view(self)
            Arranges and displays the menu elements.
        disable_buttons(self, account_button=False, direct_button=False, logout_button=False)
            Toggles the enabled/disabled state of specified buttons.

    :Attributes:
        SETTING_FONT: Font for the settings label.
        DISABLE_FONT: Font for disabled buttons.
        settings_label: Label displaying "Settings".
        canvases: List of canvases for visual separation.
        btn_account_status: Button for accessing account status.
        btn_direct_status: Button for accessing direct status (purpose unclear, yet...).
        btn_logout: Button for logging out of the application.
    """

    __MY_CANVAS_Y = 105

    def __init__(self, root, frame, canvas_total=0):
        self.root = root
        self.frame = frame
        self.canvas_total = canvas_total
        self.canvases = []
        self.SETTING_FONT = tk.CTkFont(
            family="Times New Roman", size=20, weight="bold", underline=True
        )
        self.DISABLE_FONT = tk.CTkFont(size=12, overstrike=True)

        self.settings_label = tk.CTkLabel(
            self.frame, text="Settings", font=self.SETTING_FONT
        )

        self.canvases = self.canvas_creator(
            self.frame,
            total=self.canvas_total,
            width=90,
            height=6,
            highlight_background="gray85",
            bg="gray85",
        )

        self.btn_account_status = tk.CTkButton(
            self.frame,
            text="Account status",
            text_color=("black", "white"),
            width=95,
            height=25,
            hover_color=("gray75", "gray35"),
            font=("Bold", 12),
        )
        self.btn_direct_status = tk.CTkButton(
            self.frame,
            text="Direct status",
            text_color=("black", "white"),
            width=95,
            height=25,
            font=("Bold", 12),
            anchor=tk.W,
        )
        self.btn_logout = tk.CTkButton(
            self.frame,
            text="Logout",
            text_color=("black", "white"),
            width=93,
            height=25,
            font=("Bold", 12),
            anchor=tk.W,
        )

    def create_view(self) -> None:
        """Packs and positions GUI elements on the screen."""
        self.settings_label.place(anchor=tk.CENTER, relx=0.5, y=25)
        self.btn_account_status.place(anchor=tk.W, x=8, y=130)
        self.btn_direct_status.place(anchor=tk.W, x=8, y=180)
        self.btn_logout.place(anchor=tk.W, x=8, y=230)

        if self.canvases:
            for canvas in self.canvases:
                canvas.place(anchor=tk.W, x=8, y=self.__MY_CANVAS_Y)
                canvas.create_line(3, 5, 90, 5, width=1, fill="gray55")
                self.__MY_CANVAS_Y += 50

    def disable_buttons(
            self, account_button=False, direct_button=False, logout_button=False
    ) -> None:
        """Toggles the enabled/disabled state of specified buttons.
        :Usage:
            Because the buttons are permanently placed in the settings menu.
             This function is made for activation and deactivation in login and home mode
        """
        if account_button:
            self.btn_account_status.configure(state=tk.DISABLED, font=self.DISABLE_FONT)
        else:
            self.btn_account_status.configure(
                state=tk.NORMAL,
                font=("Bold", 12),
            )

        if direct_button:
            self.btn_direct_status.configure(state=tk.DISABLED, font=self.DISABLE_FONT)
        else:
            self.btn_direct_status.configure(
                state=tk.NORMAL,
                font=("Bold", 12),
            )

        if logout_button:
            self.btn_logout.configure(state=tk.DISABLED, font=self.DISABLE_FONT)
        else:
            self.btn_logout.configure(
                state=tk.NORMAL,
                font=("Bold", 12),
            )
