import customtkinter as tk
from front.utils.constructor_class import ConstructorClass


class ToplevelWindow(tk.CTkToplevel, ConstructorClass):
    """
    This module defines the ToplevelWindow class, responsible for creating and managing a scrollable top-level window within a Customtkinter application.

    Key features:
        Customizable appearance: Employs customtkinter for enhanced visual appeal.
        Dynamic content: Adapts its layout based on the provided status list.
        Scrollable content: Enables viewing content that exceeds the window's initial size.

    Classes:
        ToplevelWindow(tk.CTkToplevel, ConstructorClass)

    Methods:
        init(self)
            Initializes the ToplevelWindow instance.
        get_toplevel_view(self, status_list)
            Configures the window's content based on the provided status list.

    Attributes:
        scroll_frame: A scrollable frame to hold the content.
        nothing_label: A label displayed when no content is available.
    """

    def __init__(self):
        super().__init__()
        self.geometry("250x300")
        self.title("View list")
        self.scroll_frame = tk.CTkScrollableFrame(self)
        self.scroll_frame.pack(fill="both", expand=True)
        self.nothing_label = tk.CTkLabel(
            self.scroll_frame,
            text="Nothing to show...",
            text_color=("black", "white"),
            fg_color="transparent",
        )

    def get_toplevel_view(self, status_list):
        """Configures the window's content based on the provided status list."""
        length = len(status_list)
        if length > 0:
            frames = self.frame_creator(
                self.scroll_frame, length, width=250, height=15, fg_color="transparent"
            )
            for frame in frames:
                frame.pack(pady=10)
            names_label = self.label_creator(
                frames,
                status_list,
            )
            for name in names_label:
                name.place(anchor=tk.CENTER, relx=0.5, rely=0.5)
        else:
            self.nothing_label.pack(pady=30)
        return self
