import os
from PIL import Image
from configs.path import BASE_PATH
from front.utils.image_utils.image_tools import image_converter, image_finder

BACK_GROUND = Image.open(os.path.join(BASE_PATH, "front", "src", "images", "insta.png"))

PASSWORD_HIDE_IMAGE_BLACK = Image.open(
    os.path.join(
        BASE_PATH, "front", "src", "images", "password-state", "black-hide.png"
    )
)
PASSWORD_HIDE_IMAGE_WHITE = Image.open(
    os.path.join(
        BASE_PATH, "front", "src", "images", "password-state", "white-hide.png"
    )
)
PASSWORD_VIEW_IMAGE_BLACK = Image.open(
    os.path.join(
        BASE_PATH, "front", "src", "images", "password-state", "black-view.png"
    )
)
PASSWORD_VIEW_IMAGE_WHITE = Image.open(
    os.path.join(
        BASE_PATH, "front", "src", "images", "password-state", "white-view.png"
    )
)

BLACK_SETTINGS_IMAGE = Image.open(
    os.path.join(BASE_PATH, "front", "src", "images", "settings", "black-setting.png")
)
WHITE_SETTINGS_IMAGE = Image.open(
    os.path.join(BASE_PATH, "front", "src", "images", "settings", "white-setting.png")
)

BLACK_MODE = Image.open(
    os.path.join(
        BASE_PATH, "front", "src", "images", "dark-light-mode", "black-mode.png"
    )
)
WHITE_MODE = Image.open(
    os.path.join(
        BASE_PATH, "front", "src", "images", "dark-light-mode", "white-mode.png"
    )
)


def profile_image_getter():
    """Retrieves a profile image, prepares it for use, and returns it as an Image object.

    Actions:
        1. Locates a profile image within a specified directory using image_finder().
        2. If a file named "profile.png" already exists in the directory, it is removed.
        3. The located image is renamed to "profile.png".
        4. The "profile.png" image is converted to a circular format with a minimum dimension of 170 pixels using image_converter().
        5. The converted image is opened as an Image object using PIL.

    :returns:An Image object representing the prepared profile image."""
    __image_name = image_finder(
        os.path.join(BASE_PATH, "front", "src", "images", "profile")
    )
    if os.path.exists(
        os.path.join(BASE_PATH, "front", "src", "images", "profile", "profile.png")
    ):
        os.remove(
            os.path.join(BASE_PATH, "front", "src", "images", "profile", "profile.png")
        )

    os.rename(
        os.path.join(BASE_PATH, "front", "src", "images", "profile", __image_name),
        os.path.join(BASE_PATH, "front", "src", "images", "profile", "profile.png"),
    )

    image_converter(
        os.path.join(BASE_PATH, "front", "src", "images", "profile", "profile.png")
    )

    profile = Image.open(
        os.path.join(BASE_PATH, "front", "src", "images", "profile", "profile.png")
    )
    return profile
