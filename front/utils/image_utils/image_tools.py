from PIL import Image, ImageDraw
import os


def image_converter(path_to_image: str) -> None:
    """
    Converts an image to a circular format and resizes it to a minimum dimension of 170 pixels.

    :param path_to_image: The path to the image file to be converted.
    :return: The original image file at the specified path is overwritten with the converted image.
    """
    img = Image.open(path_to_image)
    width, height = img.size
    min_dimension = 170

    circle_mask = Image.new("L", (width, height), 0)
    draw = ImageDraw.Draw(circle_mask)
    draw.ellipse((0, 0, width, height), fill=255)

    circular_img = img.copy()
    circular_img.putalpha(circle_mask)

    circular_img = circular_img.resize((min_dimension, min_dimension))

    circular_img.save(path_to_image, format="PNG")


def image_finder(path_to_directory) -> str:
    """
    Locates a PNG or JPG image within a specified directory.

    :param path_to_directory:
    :return: The name of the first PNG or JPG image found in the directory, or None if no such image is found.
    """
    with os.scandir(path_to_directory) as it:
        for entry in it:
            if entry.name.endswith(".png") or entry.name.endswith(".jpg"):
                name = entry.name
                break
    return name
