import os
from functools import wraps
from front.utils.messagesBox import MessageBoxClass
from configs.path import BASE_PATH
from configs.reg import register


def user_pass_validator(func):
    @wraps(func)
    def wrapper(self, username, password):
        if username == "" or password == "":
            MessageBoxClass.raise_box_error(
                "Validate Error", "username or password is required"
            )
        elif len(password) < 6:
            MessageBoxClass.raise_box_error("Validate Error", "password is too short")
        else:
            return func(self, username, password)

    return wrapper


def retries(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        for i in range(5):
            register()
            print(f"retries for {i} times ..")
            try:
                func(*args, **kwargs)
            except Exception:
                os.rmdir(os.path.join(BASE_PATH, "data"))
            else:
                break

    return wrapper
