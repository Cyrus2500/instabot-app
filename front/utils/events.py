import time
from front.utils.messagesBox import MessageBoxClass
from backend.utils.instagram import Instagram


class EventHandlerMixin:
    """
    Provides event handling functionality, particularly for application closure events.

    Methods:
        on_close(app, data)
    """

    @staticmethod
    def on_close(app, data):
        """
        Handles the application's close event, managing data storage and user confirmation.
        :param:
            app: The application object being closed.
            data: Optional data to be stored before exiting.

        Actions:
            Displays a confirmation message box to prompt the user to confirm exit.
            If data is provided:
            If the user chooses "Cancel", no action is taken.
            If the user chooses "Exit":
            Delays closure for 1.5 seconds.
            Stores the provided data using Instagram.store.apply_new_data().
            Destroys the application.
            If no data is provided, destroys the application immediately.
            Intended for use as a mixin to add event handling capabilities to other classes.
        """
        msg = MessageBoxClass.raise_question(
            "Confirm exit", "Are you sure you want to exit?"
        )
        if data is not None:
            if msg.get() == "Cancel":
                pass
            elif msg.get() == "Exit":
                time.sleep(1.5)
                Instagram.store.apply_new_data(data)
                app.destroy()
        else:
            app.destroy()
