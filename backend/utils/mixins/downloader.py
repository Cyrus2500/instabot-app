from backend.utils.mixins import BaseClassMixin
import shutil
from configs.path import BASE_PATH
import os


class DownloaderMixin(BaseClassMixin):
    """
    A mixin class for all kinds of downloads about Instagram

    New methods:
        soon ...

    Attributes:
        source_path (str): The default profile downloaded path by 'instaloader'.download_profile.
        destination_path (str): The path that will contain the downloaded profile.
        profile_dir (str): The downloaded profile directory by 'instaloader'.download_profile.

    Methods:
        public's:
            download_only_profile ()
        private's:
            __move_to_folder()
    """

    source_path = None
    destination_path = os.path.join(BASE_PATH, "front", "src", "images", "profile")
    profile_dir = None

    @classmethod
    def download_only_profile(cls, username: str) -> bool and str:
        """
        downloading profile image with 'instaloader'.download_profile method.
        When the profile is successfully downloaded, the move_to_folder method is called ...

        :param username: username of the account.
        :return: If an error occurs during the download, an error 'msg' and 'False' will be returned,
                 otherwise 'True' and 'None' .
        """
        # os.chdir(BASE_PATH)
        cls.profile_dir = os.path.join(BASE_PATH, "data", str(username))
        try:
            cls.loder.download_profile(
                username, profile_pic_only=True, profile_pic=True
            )
        except Exception as err:
            return False, str(err)
        else:
            cls.__move_to_folder()
            return True, ""

    @classmethod
    def __move_to_folder(cls) -> None:
        """Moving downloaded profile image to 'front/src/images/profile' folder."""
        with os.scandir(cls.profile_dir) as it:
            for entry in it:
                if entry.name.endswith(".png") or entry.name.endswith(".jpg"):
                    cls.source_path = entry.path
                    break
        shutil.move(cls.source_path, cls.destination_path)
