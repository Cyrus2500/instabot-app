from abc import ABC
import instaloader


class BaseClassMixin(ABC):
    """
    A mixin class only for inheritance. contains internal methods to
    simplify the syntax in the inherited classes.
    """

    loder = instaloader.Instaloader()
    exceptions = instaloader.exceptions

    @classmethod
    def login(cls, user, passw):
        cls.loder.login(user, passw)

    @classmethod
    def profile(cls, username):
        return instaloader.Profile.from_username(cls.loder.context, username)
