import os
from configs.path import BASE_PATH
from backend.utils.mixins import BaseClassMixin


class SessionManagerMixin(BaseClassMixin):
    @classmethod
    def save_info(cls) -> None:
        os.chdir(os.path.join(BASE_PATH, "data"))
        with open("session.txt", "w"):
            cls.loder.save_session_to_file("session.txt")

    @classmethod
    def load_info(cls, username) -> None:
        """load user session info for automatic login ..."""
        os.chdir(os.path.join(BASE_PATH, "data"))
        cls.loder.load_session_from_file(username=username, filename="session.txt")
