import sys
from backend.utils.mixins import BaseClassMixin


class InstaCheckClassMixin:
    """
    Contain check methods such as:
        both, followers_count, followings_count, not back followers_by_name, mutual followers_by_name.
    """

    __slots__ = ["__user", "__profile", "__followers_by_name", "__followings_by_name"]
    base = BaseClassMixin

    def __init__(self, user):
        self.__user = user
        self.__profile = self.base.profile(username=self.__user)
        self.__followers_by_name = self._check_followers_name()
        self.__followings_by_name = self._check_followings_name()

    @classmethod
    def get_user(cls, username):
        """getting user and initiating class..."""
        return cls(username)

    def _check_followers_name(self) -> list:
        try:
            followers = self.__profile.get_followers()
        except Exception as err:
            print(str(err))
            sys.exit()

        names = [follower.username for follower in followers]

        return names

    def _check_followings_name(self) -> list:
        try:
            followings = self.__profile.get_followees()
        except Exception as err:
            print(str(err))
            sys.exit()

        names = [following.username for following in followings]

        return names

    @property
    def media_count(self) -> int:
        return self.__profile.mediacount

    @property
    def followers_count(self) -> int:
        return self.__profile.followers

    @property
    def followings_count(self) -> int:
        return self.__profile.followees

    @property
    def followers_by_name(self) -> list:
        return self.__followers_by_name

    @property
    def followings_by_name(self) -> list:
        return self.__followings_by_name
