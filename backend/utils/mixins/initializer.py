from backend.utils.datastore import StoreClass
from backend.utils.mixins import InstaCheckClassMixin


class InitializerMixin:
    insta_check_methods = InstaCheckClassMixin
    __slots__ = ["_user", "check_methods"]

    def __init__(self, user):
        self._user = user
        self.check_methods = self.insta_check_methods.get_user(self._user)

    def initial_site_data(self, store_type="file", store: bool = False) -> dict:
        """
        Show all information about follower|followings count, name and not follow back people.

        :param store: data will be saved if True, else: it's just will be  shown.
        :param store_type: select a type of file to save data.
        :return: dict.
        """
        data = dict(
            user=self._user,
            followers_count=self.check_methods.followers_count,
            following_count=self.check_methods.followings_count,
            post_count=self.check_methods.media_count,
            followers_name=self.check_methods.followers_by_name,
            followings_name=self.check_methods.followings_by_name,
        )

        if store:
            store = StoreClass().set_store(store_type)
            store.save(data)
            print("\ndata successfully saved!..")
        return data
