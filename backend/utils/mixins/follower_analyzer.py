from backend.utils.datastore import StoreClass


class FollowerAnalyzerMixin:
    __slots__ = ["__site_data", "__load_and_store_type", "__loaded_data"]

    def __init__(self, site_data, load_and_store_type="file"):
        self.__site_data: dict = site_data
        self.__load_and_store_type = load_and_store_type
        self.__loaded_data: dict = self._read_data()

    def _read_data(self) -> dict:
        """
        Only use for read stored data.
        from:
            database or json file.
        :return: dict.
        """
        loader = StoreClass.set_loder(self.__load_and_store_type)
        read_data = loader.load()
        return read_data

    def unfollow_count(self) -> int:
        """
        Checking the difference between 'new data' and 'stored data'.

        :return: False if there is no difference between loaded data from 'database/jsonfile'
                and new data, else return True.

        """
        load_count = int(self.__loaded_data.get("followers_count", 0))
        site_count = int(self.__site_data.get("followers_count", 0))
        if site_count < load_count:
            return load_count - site_count
        else:
            return 0

    def new_follow_count(self) -> int:
        """
        Checking the difference between 'new data' and 'stored data'.

        :return: False if there is no difference between loaded data from 'database/jsonfile'
                and new data, else return True.

        """
        load_count = int(self.__loaded_data.get("followers_count", 0))
        site_count = int(self.__site_data.get("followers_count", 0))
        if site_count > load_count:
            return site_count - load_count
        else:
            return 0

    def none_follow_count(self) -> int:
        not_following_back: list = list(
            set(self.__site_data.get("followings_name", ""))
            - set(self.__site_data.get("followers_name", ""))
        )

        return len(not_following_back)

    def mute_follow_count(self) -> int:
        mutual_followers: list = list(
            set(self.__site_data.get("followers_name")).intersection(
                self.__site_data.get("followings_name")
            )
        )

        return len(mutual_followers)

    def unfollow_list(self) -> list:
        """
        Getting the followers names that unfollowed you recently.

        :return: list of names if diff_unfollow is not 0, else return False
        """
        load_count = int(self.__loaded_data.get("followers_count", 0))
        site_count = int(self.__site_data.get("followers_count", 0))
        if load_count > site_count:
            users = set(self.__loaded_data.get("followers_name")) - set(
                self.__site_data.get("followers_name")
            )
            return list(users)
        return []

    def new_follow_list(self) -> list:
        load_count = int(self.__loaded_data.get("followers_count", 0))
        site_count = int(self.__site_data.get("followers_count", 0))
        if load_count < site_count:
            users = set(self.__site_data.get("followers_name")) - set(
                self.__loaded_data.get("followers_name")
            )
            return list(users)
        return []

    def none_follow_list(self) -> list:
        not_following_back: list = list(
            set(self.__site_data.get("followings_name"))
            - set(self.__site_data.get("followers_name"))
        )
        return not_following_back

    def mutual_follow_list(self) -> list:
        mutual_followers: list = list(
            set(self.__site_data.get("followers_name")).intersection(
                self.__site_data.get("followings_name")
            )
        )
        return mutual_followers
