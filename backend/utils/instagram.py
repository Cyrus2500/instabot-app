from backend.utils.mixins import (
    BaseClassMixin,
    InitializerMixin,
    SessionManagerMixin,
    FollowerAnalyzerMixin,
    InstaCheckClassMixin,
    DownloaderMixin,
)
from backend.utils.datastore import StoreClass


class Instagram(BaseClassMixin):
    """
    Instagram class

    The Instagram class is a combination class that defines all the base classes and
        mixins for their final use in its features.

    Functions:
        mixins functions ...

    Attributes:
        - base : an instance of the BaseClassMixin class providing additional functionality.
        - initial : an instance of the Initializer class providing additional functionality.
        - data_manager : an instance of the DataManager class providing additional functionality.
        - analyzer : an instance of the Analyzer class providing additional functionality.
        - checker : an instance of the Checker class providing additional functionality.

        Note :
            To know how each class works, refer to the class's own document.

    Usage:
    1. Create an instance of the Instagram class.
    2. Use the various features implemented in the Combiner class, which leverage the functionalities provided by the
        base classes and mixins.

    Example:
        ```
        combiner = Instagram()

        combiner.base.login() | ...
        combiner.initial.initial_site_data() ... # store=True | False
        combiner.data_manager.save_info() |  ...
        combiner.analyzer.search_unfollow() |  ...
        combiner.checker.followers_count | ...
        combiner.store.apply_new_data | ...
        ```

    Note:
        The Instagram class is designed to combine and utilize the functionalities of the underlying classes and
            mixins, providing a unified interface for the final usage.
    """

    initial = InitializerMixin
    data_manager = SessionManagerMixin
    analyzer = FollowerAnalyzerMixin
    checker = InstaCheckClassMixin
    store = StoreClass
    downloader = DownloaderMixin
