import json
import os
from backend.utils.datastore.mongo import MongoBase
from configs.path import BASE_PATH


class FileStorage:
    @staticmethod
    def save(data):
        os.chdir(os.path.join(BASE_PATH, "data"))
        with open(R"UserInfo.json", "w") as file:
            file.write(json.dumps(data))

    @staticmethod
    def load():
        os.chdir(os.path.join(BASE_PATH, "data"))
        with open(R"UserInfo.json", "r") as file:
            data = file.read()
        return json.loads(data)


class MongoStorage:
    def __init__(self):
        self.mongo = MongoBase()

    def save(self, data):
        collection = self.mongo.data_base["UserInfo"]
        if isinstance(data, list) and len(data) > 1:
            collection.insert_many(data)
        else:
            collection.insert_one(data)

    def load(self):
        try:
            collection = self.mongo.data_base.get_collection("UserInfo")
        except Exception:
            print("collection dose not exist!.")
            return
        else:
            return collection.find()
