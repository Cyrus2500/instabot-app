"""
__version__ = rc.1-0.0.1(Pre-release)

The instabot main page contains the following:

    User profile image: This image is taken from the user's Instagram account and placed at the top of the page.
    Username: The user's Instagram username is placed below the profile image.
    Number of posts, followers, and followings: These numbers are placed below the username.
    Follower lists: There are four follower lists on this page:
    The unfollow list includes people who the user follows, but who do not follow the user.
    The none follow list includes people who the user does not follow, but who follow the user.
    The mutual follow list includes people who the user they follow each other.
    The new follow list includes people who have recently followed the user.
    Buttons to view follower lists: To view any of the follower lists, you can click on the corresponding button.
    When the main page loads, the program first retrieves the user's information from their Instagram account. Then,
     the user's profile image is taken and placed on the page. Finally, the number of posts, followers, and followings of the user are displayed on the page.
    To view the follower lists, you can click on the corresponding button. This will open a new window and display the desired follower list in it.

    Here are some additional details about the main page:
        The profile image is displayed in a circular frame with a white background.
        The username is displayed in a large, bold font.
        The numbers of posts, followers, and followings are displayed in a smaller font.
        The follower lists are displayed in four frames below the username and numbers.
        The buttons to view the follower lists located below the follower lists.
"""

from front.main_view import MainView

if __name__ == "__main__":
    MainView().mainloop()
