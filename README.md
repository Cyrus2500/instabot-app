# InstaBot
InstaBot is a desktop program that provides a bot for Instagram, with features such as unfollowing users, managing direct messages, and managing accounts. The program is currently in its initial development stage and will soon have additional features for direct messages and account management.

## Features :
* Unfollower :

    The bot can report mutual followers and provide reports on new followers, followers who unfollowed the user, and followers who the user doesn't follow back.


* Direct Message Management (Coming soon) :

  The bot will soon have the ability to manage direct messages, including sending, receiving, and organizing them.


* Account Management (Coming soon) :

    The bot will soon provide features for managing Instagram accounts, such as updating profile information, changing passwords, and more.

## Installation :
    $ git clone https://gitlab.com/silence5043629/instabot-app.git

Change into the instabot directory:

    $ cd instabot-app
Install the required dependencies:

    $ pip install -r requirements.txt

Usage:

    $ python3 main.py




## Contributing :
Thank you for considering contributing to InstaBot! If you would like to contribute, please follow these guidelines:

1. Fork the repository and create a new branch for your feature or bug fix.

2. Make your changes and ensure that the code passes all tests.

3. Write clear and concise commit messages.

4. Push your branch to your forked repository.

5. Submit a pull request, describing your changes and explaining why they should be merged.

## License :
* soon ...

## Contact us :
If you have any questions or suggestions regarding InstaBot, please feel free to contact us at [@Cyrus2500Py] on telegram

We appreciate your interest in InstaBot and welcome any feedback or contributions you may have. Thank you!